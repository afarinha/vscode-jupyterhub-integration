# Working Locally

One way to improve your workflow is to edit Jupyter notebooks using your local editing environment while running the processing outside of your computer. To accomplish this we can use the [visual studio code](https://code.visualstudio.com/) editor (usually just VSCode) connected to [CERN's JupyterHub](https://hub.cern.ch). VSCode allows one-click installation of [numerous extensions](https://marketplace.visualstudio.com/VSCode) from  within the application that can help you do your work faster and with less mistakes.

## Connecting to [CERN's JupyterHub](https://hub.cern.ch) using VSCode

### **1 - Get your JupyterHub Access Token**

1. Go to hub.cern.ch and login with your CERN account.

2. On the top left of the webpage click on `Token`, this will redirect you to a page where you should press the `Request new API token` (you may add a note to identify this token to better keep track of what it is for).

3. Copy the token and store it as if it were a password to your JupyterHub, revoking it when it is no longer needed.


### **2 - Connect VSCode to JupyterHub**

> Note: this part requires that the [Python extension](https://marketplace.visualstudio.com/items?itemName=ms-python.python) for VSCode is installed.

1. Open the command palette in VSCode by pressing `Ctrl+Shift+P` (or `Cmd+Shift+P` on MacOS). Alternatively, on the top bar click `View > Command Palette...`.

2. Start typing and select `Python: Specify local or remote Jupyter server for connections` and then select `Existing`.

3. Build your JupyterHub server URL with the following format:
    ```
    https://hub.cern.ch/user/<CERN-username>/?token=<your-token>
    ```

    for example:
    ```
    https://hub.cern.ch/user/afarinha/?token=2bb0c2476a6b4a8b8f1cdb2ffa2d97cd
    ```

### **3 - Test it: Create or Open a Notebook**

> Note: for this part you need to install an extension that renders notebooks the way we are used to, with cells and buttons to interact with them, for example, the [jupyter-notebook-vscode](https://marketplace.visualstudio.com/items?itemName=samghelms.jupyter-notebook-vscode) extension; otherwise when we open a `.ipynb` file we will simply see jumbled text.

1. Ensure that your JupyterHub server is running: go to the [hub's home page](https://hub.cern.ch/hub/home), click on `Start My Server` and follow the instructions on the next web page.
    
    > (Practical example) `Start My Server` redirects you to a new web page where you then click on `Launch Server` and get redirected again. In the `Image` section, first select the `Custom` radio button and then replace the text in the box with: gitlab-registry.cern.ch/binder/images/build-https-3a-2f-2fgitlab-2ecern-2ech-2fafarinha-2fnbresuse-2dextension-433ee8:22a057f0ea63f01ea56a71d009835f9eb22ba3c8. Finally, press the orange `Spawn` button and wait for the web page to load &ndash; you will be redirected to a personal JupyterLab server.

2. Open an existing notebook in VSCode, press the rocket icon in the upper right-hand corner of the editor to render the notebook into the format we are used to and then run a cell containing `!hostname` which should result in an output like: `jupyter-<CERN-username>`. If your notebook is not displayed correctly at first, try opening it in a new tab, or closing it and re-opening.

    > (Practical example) If you want to test this but you do not have a notebook, you can download [this one](https://gitlab.cern.ch/afarinha/nbresuse-extension/blob/master/example.ipynb), navigate to its directory in VSCode, open it like explained above and run the cell that runs a CPU workload, or create a new one and run `!hostname`.
<!--
put images, namely for the rocket button
pontentially make available a simpler example or one that relies on the base image (base image is still to be decided)
-->

This section of the documentation was was inspired by the "Connect to a JupyterHub from Visual Studio Code" [blog post](https://blog.jupyter.org/connect-to-a-jupyterhub-from-visual-studio-code-ed7ed3a31bcb).